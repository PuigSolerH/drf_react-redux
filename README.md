# React-Redux & React-MobX / Django Rest Framework

Glassy is an app made with MVC structure with Laravel and Angular6
The main purpose of this appliaction is to get into those programming languages at class and to make a project using them.


## Getting Started

This is a simple application with some user funcionality such as Login, Register, Social Login, Listing Products and filtering them where we catch that data from DRF server.


### Prerequisites

First of all you will need to execute the following commant to install what am I using in this app.

NPM
```
sudo apt-get install npm
```

[pyenv](https://github.com/yyuu/pyenv#installation).
[pyenv-virtualenv](https://github.com/yyuu/pyenv-virtualenv#installation).
Python 3.5.2: `pyenv install 3.5.2`.


### Installing

You will need to execute the follwing commands to run the development env once you have installed the prerequisites.

On Reaxct-Redux and React-MobX

```
cd react-redux OR cd react-mobx
npm install
npm start
```

On server

```
cd django-realworld-example-app
pyenv virtualenv 3.5.2 productionready
pyenv local productionready
pyenv rehash
(If your command line prompt does not start with `(productionready)` at this point, try running `pyenv activate productionready` or `cd ../productionready-django-api`.) 
```


## Built With

* [Laravel](https://laravel.com/) - As a backend framework
* [Angular](https://angular.io/) - As a frontend framework
* [NPM](https://www.npmjs.com/) - Dependency Management
* [PHPMyAdmin](https://www.phpmyadmin.net/) - DB used
* [Babel](https://babeljs.io/) - JavaScript compiler


## Authors

* **Hibernon Puig** - [Puigsolerh](https://github.com/puigsolerh)


## Extra Functionality

On DRF:
* Graphene Queries and Mutations
* New view for social login
* Filler.py for generating fake data.
* Contact module for sending emails.

On React-Redux:
* Apollo Graphql Queries and Mutations
* Delete Button component which uses mutations
* Noticia component for each team and the details of it
* React Hooks
* Contact with toastr and validation.

On MobX:
* Contact with toastr and validation.


## Extra

This app is incomplete (like everyonse should be). So I could have include extra functionality such as favorite list, creating teams and news and so on.
