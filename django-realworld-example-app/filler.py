import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE','conduit.settings')

import django
django.setup()

from conduit.apps.equipos.models import Equipo, Deporte, Noticia
from conduit.apps.profiles.models import Profile
from conduit.apps.authentication.models import User
from faker import Faker

fake = Faker()

def generateDummies(n):
    for i in range (n):
        fake_slug = fake.slug()
        fake_nombre = fake.word()
        deporte = Deporte.objects.get_or_create(slug=fake_slug, nombre=fake_nombre)[0]

        fake_slug = fake.slug()
        fake_nombre = fake.word()
        fake_presupuesto = fake.random_number(digits=9)
        fake_victorias = fake.random_number(digits=2)
        fake_empates = fake.random_number(digits=2)
        fake_derrotas = fake.random_number(digits=2)
        fake_puntos = fake.random_number(digits=2)
        author_hiber = Profile.objects.get_or_create(user__username='hiber')[0]
        equipo = Equipo.objects.get_or_create(slug=fake_slug, nombre=fake_nombre, presupuesto=fake_presupuesto, victorias=fake_victorias, empates=fake_empates, derrotas=fake_derrotas, puntos=fake_derrotas, user=author_hiber)[0]
        equipo.deporte.add(deporte)

        fake_title = fake.word()
        fake_body = fake.text()
        noticia = Noticia.objects.get_or_create(title=fake_title, body=fake_body, author=author_hiber)[0]
        noticia.equipo.add(equipo)



if __name__ == '__main__':
    print("Filling random data")
    generateDummies(4)
    print("Filling done ")