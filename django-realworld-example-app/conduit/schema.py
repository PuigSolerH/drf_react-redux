import conduit.apps.equipos.schema
import conduit.apps.profiles.schema

import graphene

from graphene_django.debug import DjangoDebug

class Query(conduit.apps.equipos.schema.Query,
            conduit.apps.profiles.schema.Query,
            graphene.ObjectType):
    debug = graphene.Field(DjangoDebug, name='__debug')

class Mutation(conduit.apps.equipos.schema.Mutation):
    debug = graphene.Field(DjangoDebug, name='__debug')


schema = graphene.Schema(query=Query, mutation=Mutation)
