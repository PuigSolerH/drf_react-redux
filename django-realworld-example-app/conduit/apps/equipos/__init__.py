from django.apps import AppConfig


class EquiposAppConfig(AppConfig):
    name = 'conduit.apps.equipos'
    label = 'equipos'
    verbose_name = 'Equipos'

    def ready(self):
        import conduit.apps.equipos.signals

default_app_config = 'conduit.apps.equipos.EquiposAppConfig'
