from conduit.apps.core.renderers import ConduitJSONRenderer


class EquiposJSONRenderer(ConduitJSONRenderer):
    object_label = 'equipos'
    pagination_object_label = 'equipos'
    pagination_count_label = 'equiposCount'


class NoticiasJSONRenderer(ConduitJSONRenderer):
    object_label = 'noticia'
    pagination_object_label = 'noticias'
    pagination_count_label = 'noticiasCount'