from .models import Equipo, Noticia, Deporte
# from graphene import ObjectType, Node, Schema
# from graphene_django.fields import DjangoConnectionField
from graphene_django import DjangoObjectType
import graphene

class DeporteNode(DjangoObjectType):

    class Meta:
        model = Deporte
        # interfaces = (Node, )

class EquipoNode(DjangoObjectType):

    class Meta:
        model = Equipo
        # interfaces = (Node, )

class NoticiaNode(DjangoObjectType):

    class Meta:
        model = Noticia
        # interfaces = (Node, )

class DeleteNoticia(graphene.Mutation):
    class Input:
        noticia_pk = graphene.Int()

    noticia = graphene.Field(NoticiaNode)

    @classmethod
    def mutate(cls, root, info, noticia_pk, client_mutation_id=None):
        delete_noticia = Noticia.objects.get(pk=noticia_pk)
        delete_noticia.delete()
        return DeleteNoticia(noticia_pk)


class Query(graphene.ObjectType):
    # LIST ALL
    equipos_all = graphene.List(EquipoNode)
    deportes_all = graphene.List(DeporteNode)
    noticias_all = graphene.List(NoticiaNode)

    # LIST ONE / DETAILS
    equipo = graphene.Field(EquipoNode,
                            slug=graphene.String())
                    
    deporte = graphene.Field(DeporteNode,
                            slug=graphene.String())

    noticia = graphene.Field(NoticiaNode,
                            id=graphene.Int())

    # RELATIONS
    equipoNoticia = graphene.Field(graphene.List(NoticiaNode),
                                    slug=graphene.String())

    deporteEquipo = graphene.Field(graphene.List(EquipoNode),
                                    slug=graphene.String())


    # LIST ALL
    def resolve_equipos_all(self, info):
        return Equipo.objects.all()

    def resolve_deportes_all(self, info):
        return Deporte.objects.all()

    def resolve_noticias_all(self, info):
        return Noticia.objects.all()

    # LIST ONE / DETAILS
    def resolve_deporte(self, info, **kwargs):
        slug = kwargs.get('slug')

        if slug is not None:
            return Deporte.objects.get(slug=slug)

        return None

    def resolve_equipo(self, info, **kwargs):
        slug = kwargs.get('slug')

        if slug is not None:
            return Equipo.objects.get(slug=slug)

        return None

    def resolve_noticia(self, info, **kwargs):
        id = kwargs.get('id')

        if id is not None:
            return Noticia.objects.get(pk=id)

        return None

    # RELATIONS
    def resolve_equipoNoticia(self, info, **kwargs):
        slug = kwargs.get('slug')
        print(slug)

        if slug is not None:
            return Noticia.objects.filter(equipo__slug=slug)

        return None

    def resolve_deporteEquipo(self, info, **kwargs):
        slug = kwargs.get('slug')

        if slug is not None:
            return Equipo.objects.filter(deporte__slug=slug)

        return None

class Mutation(graphene.ObjectType):
    delete_noticia = DeleteNoticia.Field()

schema = graphene.Schema(query=Query, mutation=Mutation)
