from rest_framework import generics, mixins, status, viewsets
from rest_framework.exceptions import NotFound
from rest_framework.permissions import (
    AllowAny, IsAuthenticated, IsAuthenticatedOrReadOnly, IsAdminUser
)
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Equipo, Deporte, Noticia
from .renderers import EquiposJSONRenderer, NoticiasJSONRenderer
from .serializers import EquipoSerializer, DeporteSerializer, NoticiaSerializer


#Admin
class EquipoViewSetAdmin(viewsets.ModelViewSet):
    queryset = Equipo.objects.all()
    serializer_class = EquipoSerializer
    lookup_field = 'slug'
    permission_classes = (IsAuthenticated,)
    permission_classes = (IsAdminUser,)
        
#Admin
class DeporteViewSetAdmin(viewsets.ModelViewSet):
    queryset = Deporte.objects.all()
    serializer_class = DeporteSerializer
    permission_classes = (IsAuthenticated,)
    permission_classes = (IsAdminUser,)

#Admin
class NoticiaViewSetAdmin(viewsets.ModelViewSet):
    queryset = Noticia.objects.all()
    serializer_class = NoticiaSerializer
    permission_classes = (IsAuthenticated,)
    permission_classes = (IsAdminUser,)


class EquipoViewSet(mixins.CreateModelMixin, 
                     mixins.ListModelMixin,
                     mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet):

    lookup_field = 'slug'
    queryset = Equipo.objects.select_related('user', 'user__user')
    permission_classes = (IsAuthenticatedOrReadOnly,)
    renderer_classes = (EquiposJSONRenderer,)
    serializer_class = EquipoSerializer

    def get_queryset(self):
        queryset = self.queryset

        user = self.request.query_params.get('user', None)
        if user is not None:
            queryset = queryset.filter(user__user__username=user)

        deporte = self.request.query_params.get('deporte', None)
        if deporte is not None:
            queryset = queryset.filter(deporte__nombre=deporte)

        return queryset

    def create(self, request):
        serializer_context = {
            'user': request.user.profile,
            'request': request
        }
        serializer_data = request.data.get('equipo', {})

        serializer = self.serializer_class(
        data=serializer_data, context=serializer_context
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def list(self, request):
        serializer_context = {'request': request}
        page = self.paginate_queryset(self.get_queryset())

        serializer = self.serializer_class(
            page,
            context=serializer_context,
            many=True
        )

        return self.get_paginated_response(serializer.data)

    def retrieve(self, request, slug):
        serializer_context = {'request': request}

        try:
            serializer_instance = self.queryset.get(slug=slug)
        except Equipo.DoesNotExist:
            raise NotFound('A team with this slug does not exist.')

        serializer = self.serializer_class(
            serializer_instance,
            context=serializer_context
        )

        return Response(serializer.data, status=status.HTTP_200_OK)


    def update(self, request, slug):
        serializer_context = {'request': request}

        try:
            serializer_instance = self.queryset.get(slug=slug)
        except Equipo.DoesNotExist:
            raise NotFound('A team with this slug does not exist.')
            
        serializer_data = request.data.get('equipo', {})

        serializer = self.serializer_class(
            serializer_instance, 
            context=serializer_context,
            data=serializer_data, 
            partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_200_OK)


class NoticiaViewSet(generics.ListCreateAPIView):

    lookup_field = 'equipo__slug'
    lookup_url_kwarg = 'equipo_slug'
    permission_classes = (IsAuthenticatedOrReadOnly,)
    # queryset = Noticia.objects.select_related(
    #     'equipo', 'equipo__user', 'equipo__user__user',
    #     'author', 'author__user'
    # )
    queryset = Noticia.objects.all()
    renderer_classes = (NoticiasJSONRenderer,)
    serializer_class = NoticiaSerializer

    def filter_queryset(self, queryset):
        # The built-in list function calls `filter_queryset`. Since we only
        # want noticias for a specific equipo, this is a good place to do
        # that filtering.
        filters = {self.lookup_field: self.kwargs[self.lookup_url_kwarg]}

        return queryset.filter(**filters)

    def create(self, request, equipo_slug=None):
        data = request.data.get('noticia', {})
        context = {'author': request.user.profile}

        try:
            context['equipo'] = Equipo.objects.get(slug=equipo_slug)
        except Equipo.DoesNotExist:
            raise NotFound('A team with this slug does not exist.')

        serializer = self.serializer_class(data=data, context=context)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_201_CREATED)


class DeporteListAPIView(generics.ListAPIView):
    queryset = Deporte.objects.all()
    pagination_class = None
    permission_classes = (AllowAny,)
    serializer_class = DeporteSerializer

    def list(self, request):
        serializer_data = self.get_queryset()
        serializer = self.serializer_class(serializer_data, many=True)

        return Response({
            'deporte': serializer.data
        }, status=status.HTTP_200_OK)


class EquiposFeedAPIView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Equipo.objects.all()
    renderer_classes = (EquiposJSONRenderer,)
    serializer_class = EquipoSerializer

    def list(self, request):
        queryset = self.get_queryset()
        page = self.paginate_queryset(queryset)

        serializer_context = {'request': request}
        serializer = self.serializer_class(
            page, context=serializer_context, many=True
        )

        return self.get_paginated_response(serializer.data)


class NoticiaFeedAPIView(mixins.ListModelMixin,
                     mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet):

    queryset = Noticia.objects.all()
    renderer_classes = (NoticiasJSONRenderer,)
    serializer_class = NoticiaSerializer

    def list(self, request):
        queryset = self.get_queryset()
        page = self.paginate_queryset(queryset)

        serializer_context = {'request': request}
        serializer = self.serializer_class(
            page, context=serializer_context, many=True
        )

        return self.get_paginated_response(serializer.data)

    def retrieve(self, request, pk):
        serializer_context = {'request': request}

        try:
            serializer_instance = self.queryset.get(id=pk)
        except Noticia.DoesNotExist:
            raise NotFound('A news with this id does not exist.')

        serializer = self.serializer_class(
            serializer_instance,
            context=serializer_context
        )

        return Response(serializer.data, status=status.HTTP_200_OK)