from django.db import models
from django.conf import settings
from django.contrib.postgres.fields import ArrayField
from decimal import Decimal
from conduit.apps.core.models import TimestampedModel


class Equipo(models.Model):
    slug = models.SlugField(db_index=True, max_length=255, unique=True)
    nombre = models.CharField(db_index=True, max_length=255)
    presupuesto = models.DecimalField(max_digits=15, decimal_places=2, default=Decimal('0.00'))
    victorias = models.PositiveIntegerField(default=0)
    empates = models.PositiveIntegerField(default=0)
    derrotas = models.PositiveIntegerField(default=0)
    puntos = models.PositiveIntegerField(default=0)
    image = models.ImageField(upload_to = "../images", null=True, blank=True)

    # user = models.OneToOneField(
    #     'profiles.Profile', on_delete=models.CASCADE, related_name='equipos'
    # )

    user = models.ForeignKey(
        'profiles.Profile', on_delete=models.CASCADE, related_name='equipos'
    )

    deporte = models.ManyToManyField(
        'equipos.Deporte', related_name='equipos'
    )
    
    def __str__(self):
        return self.nombre

    class Meta:
        db_table = 'equipos'


# Equivalent a comments
# class Noticia(models.Model):
class Noticia(TimestampedModel):
    title = models.CharField(max_length=255, default='')
    body = models.TextField()

    equipo = models.ManyToManyField(
        'equipos.Equipo', related_name='noticias'
    )

    author = models.ForeignKey(
        'profiles.Profile', related_name='noticias', on_delete=models.CASCADE
    )

    def __str__(self):
        return self.title


# Equivalent a tags
class Deporte(models.Model):
    slug = models.SlugField(db_index=True, max_length=255, unique=True)
    nombre = models.CharField(max_length=255)

    def __str__(self):
        return self.nombre

    class Meta:
        db_table = 'deportes'
