# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2019-01-28 15:45
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0003_profile_favorites'),
        ('equipos', '0008_auto_20190125_1650'),
    ]

    operations = [
        migrations.CreateModel(
            name='Noticia',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('body', models.TextField()),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='noticias', to='profiles.Profile')),
                ('equipo', models.ManyToManyField(related_name='noticias', to='equipos.Equipo')),
            ],
        ),
    ]
