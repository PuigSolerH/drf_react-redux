# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2019-01-24 20:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('equipos', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='equipo',
            name='deporte',
        ),
        migrations.AddField(
            model_name='equipo',
            name='deporte',
            field=models.ManyToManyField(related_name='equipos', to='equipos.Deporte'),
        ),
    ]
