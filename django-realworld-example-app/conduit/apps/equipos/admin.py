from django.contrib import admin

from .models import Equipo, Deporte, Noticia

admin.site.register(Equipo)
admin.site.register(Deporte)
admin.site.register(Noticia)