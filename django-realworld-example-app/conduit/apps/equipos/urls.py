from django.conf.urls import include, url

from rest_framework.routers import DefaultRouter

from .views import (
    EquipoViewSet, EquiposFeedAPIView, DeporteListAPIView, 
    NoticiaViewSet, NoticiaFeedAPIView,
    EquipoViewSetAdmin, DeporteViewSetAdmin, NoticiaViewSetAdmin
)

from django.conf import settings
from django.conf.urls.static import static

# from graphene_django.views import GraphQLView
# from .schema import schema

router = DefaultRouter(trailing_slash=False)
router.register(r'equipos', EquipoViewSet)
router.register(r'noticias', NoticiaFeedAPIView)

#Admin
router.register(r'equipos_Admin', EquipoViewSetAdmin)  
router.register(r'deportes_Admin', DeporteViewSetAdmin)
router.register(r'noticiasAdmin', NoticiaViewSetAdmin)

urlpatterns = [
    # path('', views.ListTodo.as_view()),
    # path('<int:pk>/', views.DetailTodo.as_view()),
    # url(r'^<int:pk>/', )
    url(r'^', include(router.urls)),

    url(r'^equipos/feed/?$', EquiposFeedAPIView.as_view()),

    url(r'^deportes/?$', DeporteListAPIView.as_view()),

    url(r'^equipos/(?P<equipo_slug>[-\w]+)/noticias/?$', 
        NoticiaViewSet.as_view()),

    # url(r'^graphql', GraphQLView.as_view(graphiql=True, schema=schema)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)