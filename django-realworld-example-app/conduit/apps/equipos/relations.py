from rest_framework import serializers

from .models import Deporte


class DeporteRelatedField(serializers.RelatedField):
    def get_queryset(self):
        return Deporte.objects.all()

    def to_internal_value(self, data):
        deporte, created = Deporte.objects.get_or_create(deporte=data, slug=data.lower())

        return deporte

    def to_representation(self, value):
        return value.nombre