from rest_framework import serializers

from conduit.apps.profiles.serializers import ProfileSerializer

from .models import Equipo, Deporte, Noticia
from .relations import DeporteRelatedField


class EquipoSerializer(serializers.ModelSerializer):
    slug = serializers.SlugField(required=False)
    user = ProfileSerializer(read_only=True)
    # description = serializers.CharField(required=False)
    
    deporteList = DeporteRelatedField(many=True, required=False, source='deporte')

    # Django REST Framework makes it possible to create a read-only field that
    # gets it's value by calling a function. In this case, the client expects
    # `created_at` to be called `createdAt` and `updated_at` to be `updatedAt`.
    # `serializers.SerializerMethodField` is a good way to avoid having the
    # requirements of the client leak into our API.

    # createdAt = serializers.SerializerMethodField(method_name='get_created_at')
    # updatedAt = serializers.SerializerMethodField(method_name='get_updated_at')

    class Meta:
        model = Equipo
        fields = (
            'slug',
            'nombre',
            'presupuesto',
            'victorias',
            'empates',
            'derrotas',
            'puntos',
            'image',
            'user',
            'deporteList',
            # 'createdAt',
            # 'updatedAt',
        )

    # def get_image(self, obj):
    #     print(obj.image)
    #     if obj.image:
    #         return obj.image

    #     return obj.image

    def create(self, validated_data):
        user = self.context.get('user', None)

        deportes = validated_data.pop('deportes', [])

        equipo = Equipo.objects.create(user=user, **validated_data)

        for deporte in deportes:
            equipo.deportes.add(deporte)

        return equipo

    # def get_created_at(self, instance):
    #     return instance.created_at.isoformat()

    # def get_updated_at(self, instance):
    #     return instance.updated_at.isoformat()


class NoticiaSerializer(serializers.ModelSerializer):
    author = ProfileSerializer(required=False)

    createdAt = serializers.SerializerMethodField(method_name='get_created_at')
    updatedAt = serializers.SerializerMethodField(method_name='get_updated_at')

    class Meta:
        model = Noticia
        fields = (
            'id',
            'author',
            'title',
            'body',
            'createdAt',
            'updatedAt',
        )

    def create(self, validated_data):
        # article = self.context['article']
        author = self.context['author']
        equipos = validated_data.pop('equipos', [])

        noticia = Noticia.objects.create(author=author, equipo=equipo, **validated_data)

        for equipo in equipos:
            noticia.equipos.add(equipo)

        return noticia

        # return Noticia.objects.create(
        #     author=author, article=article, **validated_data
        # )

    def get_created_at(self, instance):
        return instance.created_at.isoformat()

    def get_updated_at(self, instance):
        return instance.updated_at.isoformat()


class DeporteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Deporte
        fields = ('nombre',)

    def to_representation(self, obj):
        return obj.nombre
