from .models import Profile
from conduit.apps.authentication.models import User

from graphene_django import DjangoObjectType
import graphene

class UserNode(DjangoObjectType):
    class Meta:
        model = User

class ProfileNode(DjangoObjectType):

    class Meta:
        model = Profile
        # interfaces = (Node, )


class Query(graphene.ObjectType):
    # LIST ALL
    profiles_all = graphene.List(ProfileNode)
    users_all = graphene.List(UserNode)

    # LIST ONE / DETAILS
    profile = graphene.Field(ProfileNode,
                            id=graphene.Int())
    user = graphene.Field(UserNode,
                            username=graphene.String())

    # LIST ALL
    def resolve_profiles_all(self, info):
        return Profile.objects.all()

    def resolve_users_all(self, info):
        return User.objects.all()

    # LIST ONE / DETAILS
    def resolve_profile(self, info, **kwargs):
        id = kwargs.get('id')

        if id is not None:
            return Profile.objects.get(pk=id)

        return None

    def resolve_user(self, info, **kwargs):
        username = kwargs.get('username')

        if username is not None:
            return User.objects.get(username=username)

        return None

schema = graphene.Schema(query=Query)