from django.core.mail import send_mail, BadHeaderError
from rest_framework import permissions, status, views, viewsets
from django.http import HttpResponse, HttpResponseRedirect
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import ContactSerializer
from rest_framework.permissions import AllowAny


class ContactAPIView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = ContactSerializer

    def post(self, request):
        result = request.data.get('data', {})

        # Notice here that we do not call `serializer.save()` like we did for
        # the registration endpoint. This is because we don't actually have
        # anything to save. Instead, the `validate` method on our serializer
        # handles everything we need.
        serializer = self.serializer_class(data=result)
        serializer.is_valid(raise_exception=True)

        email = result.get('email', None)
        subject = result.get('subject', None)
        comment = result.get('comment', None)

        # return Response(serializer.data, status=status.HTTP_200_OK)

        if email is None:
            return Response({
                    'status': 'false',
                    'message': "'email' field is missing"
                }, status=status.HTTP_400_BAD_REQUEST)
        elif subject is None:
            return Response({
                    'status': 'false',
                    'message': "'subject' field is missing"
                }, status=status.HTTP_400_BAD_REQUEST)
        elif comment is None:
            return Response({
                    'status': 'false',
                    'message': "'message' field is missing"
                }, status=status.HTTP_400_BAD_REQUEST)
            
        try:
            send_mail(subject, comment, 'hiber98@gmail.com', [email])
        except BadHeaderError:
            return Response({
                    'status': 'false',
                    'message': 'BadHeaderError for your message'
                }, status=status.HTTP_503_SERVICE_UNAVAILABLE)

        return Response({
                    'status': 'true',
                    'message': 'Success! Thank you for your message'
                }, status=status.HTTP_200_OK)
