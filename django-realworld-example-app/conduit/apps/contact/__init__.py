from django.apps import AppConfig


class ContactAppConfig(AppConfig):
    name = 'conduit.apps.contact'
    label = 'contact'
    verbose_name = 'Contact'

    # def ready(self):
    #     import conduit.apps.contact.signals

default_app_config = 'conduit.apps.contact.ContactAppConfig'
