import { observable, action } from 'mobx';
import agent from '../agent';
import toastr from 'toastr';

const TOASTR_OPTIONS = { closeButton: true, preventDuplicated: true, positionClass: 'toast-top-left'};
toastr.options = TOASTR_OPTIONS;

class ContactStore {
  @observable inProgress = false;
  @observable errors = undefined;
  @observable errorEmail = undefined;
  @observable errorSubject = undefined;
  @observable errorComment = undefined;

  @observable values = {
    email: '',
    subject: '',
    comment: '',
  };

  @action validateData(param, data) {
    if (param === 'email'){
      if (!data.match(/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$/)){
        this.errorEmail = true;
      }else{
        this.errorEmail = false;
      }
    }else if(param === 'subject'){
      if(data.length < 4){
        this.errorSubject = true;
      }else{
        this.errorSubject = false;
      }
    }else if(param === 'comment'){
      if(data.length < 20){
        this.errorComment = true;
      }else{
        this.errorComment = false;
      }
    }
  }

  @action setFormData(param, data) {
    this.values[param] = data;
  }

  @action reset() {
    this.values.email = '';
    this.values.subject = '';
    this.values.comment = '';
  }

  @action send(){
    if(this.errorEmail || this.errorSubject || this.errorComment || !this.values.email || !this.values.subject || !this.values.comment){
        toastr['error']('Error!')
    }else{
      return agent.Contact.contact(this.values.email, this.values.subject, this.values.comment)
        .catch(action((err) => {
          toastr['error'](err.response.errors)
        }))
        .finally(action(() => {
          toastr['success']('Email Sent!'); 
        }));
    }
  }

}

export default new ContactStore();
