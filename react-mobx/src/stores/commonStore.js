import { observable, action, reaction } from 'mobx';
import agent from '../agent';

class CommonStore {

  @observable appName = 'Conduit';
  @observable token = window.localStorage.getItem('jwt');
  @observable appLoaded = false;

  @observable tags = [];
  @observable isLoadingTags = false;
  @observable deportes = [];
  @observable isLoadingDeportes = false;

  constructor() {
    reaction(
      () => this.token,
      token => {
        if (token) {
          window.localStorage.setItem('jwt', token);
        } else {
          window.localStorage.removeItem('jwt');
        }
      }
    );
  }

  @action loadTags() {
    this.isLoadingTags = true;
    return agent.Tags.getAll()
      .then(action(({ tags }) => { this.tags = tags.map(t => t.toLowerCase()); }))
      .finally(action(() => { this.isLoadingTags = false; }))
  }

  @action loadDeportes() {
    this.isLoadingDeportes = true;
    return agent.Deportes.getAll()
      .then(action(({ deportes }) => { this.deportes = deportes.map(t => t.toLowerCase()); }))
      .finally(action(() => { this.isLoadingDeportes = false; }))
  }

  @action setToken(token) {
    this.token = token;
  }

  @action setAppLoaded() {
    this.appLoaded = true;
  }

}

export default new CommonStore();
