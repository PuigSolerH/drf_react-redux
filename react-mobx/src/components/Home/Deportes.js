import React from 'react';
import { Link } from 'react-router-dom';
import LoadingSpinner from '../LoadingSpinner';

const Deportes = props => {
  console.log(props);
  const deportes = props.deportes;
  if (deportes) {
    return (
      <div className="tag-list">
        {
          deportes.map(deporte => {

            return (
              <Link
                to={{
                  pathname: "/",
                  search: "?tab=deporte&deporte=" + deporte
                }}
                className="tag-default tag-pill"
                key={deporte}
              >
                {deporte}
              </Link>
            );
          })
        }
      </div>
    );
  } else {
    return (
      <LoadingSpinner />
    );
  }
};

export default Deportes;
