import { withRouter } from 'react-router-dom';
import ListErrors from './ListErrors';
import React from 'react';
import { inject, observer } from 'mobx-react';

@inject('contactStore')
@withRouter
@observer
export default class Contact extends React.Component {

  componentWillUnmount() {
    this.props.contactStore.reset();
  }

  handleData = e => {
      this.props.contactStore.setFormData(e.target.id, e.target.value)
      this.props.contactStore.validateData(e.target.id, e.target.value)
  };
  handleSubmitForm = (e) => {
    e.preventDefault();
    this.props.contactStore.send()
      .then(() => this.props.history.replace('/'));
  };

  render() {
    const { values, errors, inProgress, errorEmail, errorSubject, errorComment } = this.props.contactStore;

    return (
        <div className="auth-page">
            <div className="container page">
                <div className="row">

                <div className="col-md-6 offset-md-3 col-xs-12">
                    <h1 className="text-xs-center">Contact</h1>

                    <ListErrors errors={errors} />

                    <form onSubmit={this.handleSubmitForm}>
                    <fieldset>

                        <fieldset className="form-group">
                        <input
                            className="form-control form-control-lg"
                            type="email"
                            id="email"
                            placeholder="Email"
                            value={values.email}
                            onChange={this.handleData} />
                        </fieldset>
                        <span hidden={!errorEmail} style={{color:"red"}}>Invalid email</span>

                        <fieldset className="form-group">
                        <input
                            className="form-control form-control-lg"
                            type="text"
                            id="subject"
                            placeholder="Subject"
                            value={values.subject}
                            onChange={this.handleData} />
                        </fieldset>
                        <span hidden={!errorSubject} style={{color:"red"}}>Subject too short</span>

                        <fieldset className="form-group">
                        <input
                            className="form-control form-control-lg"
                            type="text"
                            id="comment"
                            placeholder="Comment"
                            value={values.comment}
                            onChange={this.handleData} />
                        </fieldset>
                        <span hidden={!errorComment} style={{color:"red"}}>Comment too short</span>

                        <button
                        className="btn btn-lg btn-primary pull-xs-right"
                        type="submit"
                        disabled={inProgress}>
                        Send
                        </button>

                    </fieldset>
                    </form>
                </div>

                </div>
            </div>
        </div>
    );
  }
}