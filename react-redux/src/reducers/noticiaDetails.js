import { NOTICIA_PAGE_LOADED, NOTICIA_PAGE_UNLOADED } from '../constants/actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case NOTICIA_PAGE_LOADED:
      return {
        ...state,
        noticia: action.payload[0].noticia
      };
    case NOTICIA_PAGE_UNLOADED:
      return {};
    default:
      return state;
  }
};
