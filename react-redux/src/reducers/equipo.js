import {
    EQUIPO_PAGE_LOADED,
    EQUIPO_PAGE_UNLOADED,
    ADD_NOTICIA,
    DELETE_NOTICIA
  } from '../constants/actionTypes';
  
export default (state = {}, action) => {
    switch (action.type) {
      case EQUIPO_PAGE_LOADED:
        let url_splitted = action.payload[0].equipos.image.split('/equipos');
        url_splitted = url_splitted.join('');
        return {
          ...state,
          equipo: action.payload[0].equipos,
          noticias: action.payload[1].noticias,
          equipo_image: url_splitted
        };
      case EQUIPO_PAGE_UNLOADED:
        return {};
      case ADD_NOTICIA:
        return {
          ...state,
          noticiaErrors: action.error ? action.payload.errors : null,
          noticias: action.error ?
            null :
            (state.noticias || []).concat([action.payload.noticia])
        };
      case DELETE_NOTICIA:
        const noticiaId = action.idNoticia
        return {
          ...state,
          noticias: state.noticias.filter(noticia => noticia.id !== noticiaId)
        };
      default:
        return state;
    }
};