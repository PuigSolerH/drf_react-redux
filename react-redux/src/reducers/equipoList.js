import {
    SET_PAGE,
    APPLY_DEPORTE_FILTER,
    HOME_PAGE_LOADED,
    HOME_PAGE_UNLOADED,
    CHANGE_TAB,
    PROFILE_PAGE_LOADED,
    PROFILE_PAGE_UNLOADED,
} from '../constants/actionTypes';
  
export default (state = {}, action) => {
    switch (action.type) {
      case SET_PAGE:
        return {
          ...state,
          equipos: action.payload.equipos,
          equiposCount: action.payload.equiposCount,
          currentPage: action.page
        };
      case APPLY_DEPORTE_FILTER:
        return {
          ...state,
          pager: action.pager,
          equipos: action.payload.equipos,
          equiposCount: action.payload.equiposCount,
          tab: null,
          deporte: action.deporte,
          currentPage: 0
        };
      case HOME_PAGE_LOADED:
        return {
          ...state,
          pager: action.pager,
          deportes: action.payload[0].deportes,
          equipos: action.payload[1].equipos,
          equiposCount: action.payload[1].equiposCount,
          currentPage: 0,
          tab: action.tab
        };
      case HOME_PAGE_UNLOADED:
        return {};
      case CHANGE_TAB:
        return {
          ...state,
          pager: action.pager,
          equipos: action.payload.equipos,
          equiposCount: action.payload.equiposCount,
          tab: action.tab,
          currentPage: 0,
          deporte: null
        };
      case PROFILE_PAGE_LOADED:
      case PROFILE_PAGE_UNLOADED:
      default:
        return state;
    }
};