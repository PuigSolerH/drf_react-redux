import {
	CONTACT,
	CONTACT_PAGE_UNLOADED,
	UPDATE_EMAIL_CONTACT,
	UPDATE_SUBJECT_CONTACT,
	UPDATE_COMMENT_CONTACT,
	TOASTR_OPTIONS,
	ERROR_SHORT_EMAIL_CONTACT,
	ERROR_SHORT_SUBJECT_CONTACT,
	ERROR_SHORT_COMMENT_CONTACT,
	INVALID_EMAIL_CONTACT
} from '../constants/actionTypes';
import toastr from 'toastr';

toastr.options = TOASTR_OPTIONS;

const INITIAL_STATE = {
	submitting: false,
	success: false,
	error: false,
	wrong_email: true,
	wrong_subject: true,
	wrong_comment: true,
	message: "",
	open: false
};

export default function(state = INITIAL_STATE, action) {
	switch (action.type) {
		case CONTACT:
			(action.payload.status)? toastr['success']('congrat!'): toastr['error']('Error!')
			return {
				...state,
				inProgress: false,
				errors: action.error ? action.payload.errors : null
			};
		case CONTACT_PAGE_UNLOADED:
			return {};
		case ERROR_SHORT_EMAIL_CONTACT:
			return { ...state, [action.key]: action.value, wrong_email: true, error_email: "Email too short"};
		case ERROR_SHORT_SUBJECT_CONTACT:
			return { ...state, [action.key]: action.value, wrong_subject: true, error_subject: "Subject too short"};
		case ERROR_SHORT_COMMENT_CONTACT:
			return { ...state, [action.key]: action.value, wrong_comment: true, error_comment: "Comment too short"};
		case UPDATE_EMAIL_CONTACT:
			return { ...state, [action.key]: action.value, wrong_email: false };
		case UPDATE_SUBJECT_CONTACT:
			return { ...state, [action.key]: action.value, wrong_subject: false };
		case UPDATE_COMMENT_CONTACT:
			return { ...state, [action.key]: action.value, wrong_comment: false };
		case INVALID_EMAIL_CONTACT:
			return {...state, [action.key]: action.value, wrong_email: true, error_email: "Invalid email format"};
		default:
			return state;
	}
}