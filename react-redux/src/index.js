import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import React from 'react';
import { store, history} from './store';

import { Route, Switch } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import { ApolloProvider } from 'react-apollo';
import { HttpLink } from 'apollo-link-http';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';

import App from './components/App';

const client = new ApolloClient({
  link: new HttpLink({ uri: 'http://127.0.0.1:8000/graphql' }),
  cache: new InMemoryCache()
});

ReactDOM.render((
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <ApolloProvider client={client}>
        <Switch>
          <Route path="/" component={App} />
        </Switch>
      </ApolloProvider>
    </ConnectedRouter>
  </Provider>

), document.getElementById('root'));
