import Noticia from './Noticia';
import React from 'react';

const NoticiaList = props => {
  return (
    <div>
      {
        props.noticias.map(noticia => {
          return (
            <Noticia
              noticia={noticia}
              currentUser={props.currentUser}
              slug={props.slug}
              noticiasCount={props.noticiasCount}
              key={noticia.id} />
          );
        })
      }
    </div>
  );
};

export default NoticiaList;