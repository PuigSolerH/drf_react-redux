import React, { useEffect } from 'react';
import agent from '../../agent';
import { connect } from 'react-redux';
import { NOTICIA_PAGE_LOADED, NOTICIA_PAGE_UNLOADED } from '../../constants/actionTypes';


const mapStateToProps = state => ({
    ...state.noticiaDetails,
    currentUser: state.common.currentUser
});
  
const mapDispatchToProps = dispatch => ({
    onLoad: payload => 
      dispatch({ type: NOTICIA_PAGE_LOADED, payload }),
    onUnload: () =>
      dispatch({ type: NOTICIA_PAGE_UNLOADED })
});

const NoticiaDetails = props => {
  useEffect(() => {
    props.onLoad(Promise.all([
      agent.Noticias.detailsNoticia(props.match.params.id)
    ]));
    
    return function cleanup() {
      props.onUnload();
    };
  }, [])

  if (!props.noticia) return null;
  return (
    <article>
      <h1>{props.noticia.title}</h1>
      <p>{props.noticia.body}</p>
    </article>
  )
}
  
export default connect(mapStateToProps, mapDispatchToProps)(NoticiaDetails);