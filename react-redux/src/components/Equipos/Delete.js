import React from 'react';
import { Mutation } from 'react-apollo';

const Delete = ({ id, mutation, show }) => {
    if(!show) return null;
    return(
    <Mutation mutation={mutation} variables={{ id }}>
      {deleteNoticia => (
        <button type="button" onClick={deleteNoticia}>
          Delete
        </button>
      )}
    </Mutation>
    );
};

export default Delete;