import NoticiaList from './NoticiaList';
import React, { useEffect } from 'react';
import agent from '../../agent';
import { connect } from 'react-redux';
import { EQUIPO_PAGE_LOADED, EQUIPO_PAGE_UNLOADED } from '../../constants/actionTypes';
import { Query } from 'react-apollo';


const mapStateToProps = state => ({
  ...state.equipo,
  currentUser: state.common.currentUser
});

const mapDispatchToProps = dispatch => ({
  onLoad: payload =>
    dispatch({ type: EQUIPO_PAGE_LOADED, payload }),
  onUnload: () =>
    dispatch({ type: EQUIPO_PAGE_UNLOADED })
});

const Equipo = props => {
  useEffect(() => {
    props.onLoad(Promise.all([
      agent.Equipos.get(props.match.params.id),
      agent.Noticias.forEquipo(props.match.params.id)
    ]));

    return function cleanup() {
      props.onUnload();
    };
  }, [])

  let slug = props.match.params.id

  return (
    <Query query={agent.GET_EQUIPO} variables={{ slug }}>
      {({ data, loading, error }) => {

        if (loading) return <div>Loading ...</div>;
        if (error) return <div>ERROR!</div>

        let url_splitted = data.equipo.image.split('../');
        url_splitted = "http://localhost:8000/api/" + url_splitted[1]

        return (
          <div className="article-page">
            <div className="banner">
              <div className="container">

                <h1>{data.equipo.nombre}</h1>
                <img src={url_splitted} alt="Team Logo" />

              </div>
            </div>

            <div className="container page">

              <div className="row article-content">
                <div className="col-xs-12">
                
                  <ul className="tag-list">
                    {
                      data.equipo.deporte.map(deporte => {
                        return (
                          <li
                            className="tag-default tag-pill tag-outline"
                            key={deporte.nombre}>
                            {deporte.nombre}
                          </li>
                        );
                      })
                    }
                  </ul>

                </div>
              </div>

              <hr />

              <div className="article-actions">
              </div>

              <Query query={agent.GET_EQUIPO_NEWS} variables={{ slug }}>
                {({ data, loading, error }) => {
                  if (loading) return <div>Loading ...</div>;
                  if (error) return <div>ERROR!</div>

                  return (
                    <div className="row">
                      <NoticiaList
                        currentUser={props.currentUser}
                        noticias={data.equipoNoticia || []}
                        slug={props.match.params.id}/>
                    </div>
                  );
                }}
              </Query>

            </div>
          </div>
        );
      }}
    </Query>
  );

}

export default connect(mapStateToProps, mapDispatchToProps)(Equipo);