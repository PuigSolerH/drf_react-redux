import React from 'react';
import agent from '../agent';
import { connect } from 'react-redux';
import { LOGIN_SOCIAL, TOASTR_OPTIONS } from '../constants/actionTypes';
import GoogleLogin from "react-google-login";
import toastr from 'toastr';

toastr.options = TOASTR_OPTIONS;

const mapDispatchToProps = dispatch => ({
    onClick: (username, email, id) => {
    const payload = agent.Auth.socialLogin(username, email, id);
    dispatch({ type: LOGIN_SOCIAL, payload})
    }
});

const GoogleButton = props => {
  const googleLogin = (result) => {
    const data = result.profileObj;
    console.log(data);
    props.onClick(data.name, data.email, data.googleId);
  };

  const errorLogin = () => {
    toastr['error']('ERROR!');
  }

  return (
    <GoogleLogin
      clientId="293056262830-165j1pq3p2s5ilasbbt69ugs73oc3cc6.apps.googleusercontent.com"
      autoLoad={false}
      onSuccess={googleLogin}
      onFailure={errorLogin}
      className="waves-effects waves-light btn-large btn-google">
        <i className="fa fa-google" />
        <span> Google </span>
    </GoogleLogin>
  );
};

export default connect(() => ({}), mapDispatchToProps)(GoogleButton);