import EquipoPreview from './EquipoPreview';
import ListPagination from './ListPagination';
import React from 'react';

const EquipoList = props => {
  if (!props.equipos) {
    return (
      <div className="article-preview">Loading...</div>
    );
  }

  if (props.equipos.length === 0) {
    return (
      <div className="article-preview">
        No teams are here... yet.
      </div>
    );
  }

  return (
    <div>
      {
        props.equipos.map(equipo => {
          return (
            <EquipoPreview equipo={equipo} key={equipo.slug} />
          );
        })
      }

      <ListPagination
        pager={props.pager}
        equiposCount={props.equiposCount}
        currentPage={props.currentPage} />
    </div>
  );
};

export default EquipoList;