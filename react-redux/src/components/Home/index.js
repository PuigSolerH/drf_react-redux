import Banner from './Banner';
import MainView from './MainView';
import React, { useEffect } from 'react';
import Deportes from './Deportes';
import agent from '../../agent';
import { connect } from 'react-redux';
import {
  HOME_PAGE_LOADED,
  HOME_PAGE_UNLOADED,
  APPLY_DEPORTE_FILTER
} from '../../constants/actionTypes';
import { Query } from 'react-apollo';

const Promise = global.Promise;

const mapStateToProps = state => ({
  ...state.home,
  appName: state.common.appName,
  token: state.common.token
});

const mapDispatchToProps = dispatch => ({
  onClickDeporte: (deporte, pager, payload) => {
    dispatch({ type: APPLY_DEPORTE_FILTER, deporte, pager, payload })
  },
  onLoad: (tab, pager, payload) =>
    dispatch({ type: HOME_PAGE_LOADED, tab, pager, payload }),
  onUnload: () =>
    dispatch({ type: HOME_PAGE_UNLOADED })
});

const Home = props => {
  useEffect(() => {
    const tab = props.token ? 'feed' : 'all';
    const equiposPromise = props.token ? agent.Equipos.feed : agent.Equipos.all;

    props.onLoad(tab, equiposPromise, Promise.all([agent.Deportes.getAll(), equiposPromise()]));

    return function cleanup() {
      props.onUnload();
    };
  }, [])

  return (
    <div className="home-page">

      <Banner token={props.token} appName={props.appName} />

      <div className="container page">
        <div className="row">
          <MainView />

          <div className="col-md-3">
            <div className="sidebar">

              <p>Popular Sports</p>

              <Query query={agent.GET_ALL_SPORTS}>
                    {({ data, loading, error }) => {
                      const deportes_data = [];
                      if (loading) return <div>Loading ...</div>;
                      if (error) return <div>ERROR!</div>

                      if (data.deportesAll) {
                        data.deportesAll.map(deporte => {
                          deportes_data.push(deporte.nombre);
                        })

                        return (
                          <Deportes
                            deportes={deportes_data}
                            onClickDeporte={props.onClickDeporte} />
                        );
                      }
                    }}
              </Query>

            </div>
          </div>
        </div>
      </div>

    </div>
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
