import React from 'react';
import agent from '../../agent';

const Deportes = props => {
  const deportes = props.deportes;
  if (deportes) {
    return (
      <div className="tag-list">
        {
          deportes.map(deporte => {
            const handleClick = ev => {
              ev.preventDefault();
              props.onClickDeporte(deporte, page => agent.Equipos.byDeporte(deporte, page), agent.Equipos.byDeporte(deporte));
            };

            return (
              <a
                href=""
                className="tag-default tag-pill"
                key={deporte}
                onClick={handleClick}>
                {deporte}
              </a>
            );
          })
        }
      </div>
    );
  } else {
    return (
      <div>Loading Deportes...</div>
    );
  }
};

export default Deportes;