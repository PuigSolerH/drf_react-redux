import React from 'react';
import { Link } from 'react-router-dom';

const EquipoPreview = props => {
  const equipo = props.equipo;

  return (
    <div className="article-preview">
      <div className="article-meta">
        <Link to={`/@${equipo.user.username}`}>
          <img src={equipo.user.image} alt={equipo.user.username} />
        </Link>

        <div className="info">
          <Link className="author" to={`/@${equipo.user.username}`}>
            {equipo.user.username}
          </Link>
        </div>

      </div>

      <Link to={`/equipo/${equipo.slug}`} className="preview-link">
        <h1>{equipo.nombre}</h1>
        <p>{equipo.presupuesto}</p>
        <span>Read more...</span>
        <ul className="tag-list">
          {
              equipo.deporteList.map(deporte => {
                return (
                  <li className="tag-default tag-pill tag-outline" key={deporte}>
                    {deporte}
                  </li>
                )
              })
            }
          </ul>
      </Link>
    </div>
  );
}

export default EquipoPreview;
