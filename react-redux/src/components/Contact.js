import ListErrors from './ListErrors';
import React from 'react';
import agent from '../agent';
import toastr from 'toastr';
import { connect } from 'react-redux';
import {
  CONTACT,
  CONTACT_PAGE_UNLOADED,
  UPDATE_EMAIL_CONTACT,
  UPDATE_COMMENT_CONTACT,
  UPDATE_SUBJECT_CONTACT,
  TOASTR_OPTIONS,
  ERROR_SHORT_EMAIL_CONTACT,
  ERROR_SHORT_SUBJECT_CONTACT,
  ERROR_SHORT_COMMENT_CONTACT,
  INVALID_EMAIL_CONTACT,
  REDIRECT_CONTACT,
  EMAIL_REGEX
} from '../constants/actionTypes';

toastr.options = TOASTR_OPTIONS;

const e_regex = EMAIL_REGEX;

const mapStateToProps = state => ({ ...state.contact });

const mapDispatchToProps = dispatch => ({
  onChangeEmail: value => {
    (value.length < 5) ?
      dispatch({ type: ERROR_SHORT_EMAIL_CONTACT, key: 'email', value }) :
      dispatch({ type: UPDATE_EMAIL_CONTACT, key: 'email', value })
  },
  onChangeSubject: value =>
    (value.length < 8) ?
      dispatch({ type: ERROR_SHORT_SUBJECT_CONTACT, key: 'subject', value }) :
      dispatch({ type: UPDATE_SUBJECT_CONTACT, key: 'subject', value }),
  onChangeComment: value =>
    (value.length < 20) ?
      dispatch({ type: ERROR_SHORT_COMMENT_CONTACT, key: 'comment', value }) :
      dispatch({ type: UPDATE_COMMENT_CONTACT, key: 'comment', value }),
  onSubmit: (email, subject, comment) => {
    if (!e_regex.test(String(email))) {
      // (!e_regex.test(String(email).toLowerCase()))?
      dispatch({ type: INVALID_EMAIL_CONTACT })
    } else {
      dispatch({ type: CONTACT, payload: agent.Contact.contact(email, subject, comment) })
      dispatch({ type: REDIRECT_CONTACT })
    }
  },
  onUnload: () =>
    dispatch({ type: CONTACT_PAGE_UNLOADED })
});

const Contact = props => {
  this.changeEmail = ev => props.onChangeEmail(ev.target.value);
  this.changeSubject = ev => props.onChangeSubject(ev.target.value);
  this.changeComment = ev => props.onChangeComment(ev.target.value);
  this.submitForm = (email, subject, comment) => ev => {
    ev.preventDefault();
    props.onSubmit(email, subject, comment);
    props.onUnload();
  };

  const email = props.email;
  const subject = props.subject;
  const comment = props.comment;
  return (
    <div className="auth-page">
      <div className="container page">
        <div className="row">

          <div className="col-md-6 offset-md-3 col-xs-12">
            <h1 className="text-xs-center">Contact</h1>

            <ListErrors errors={props.errors} />

            <form onSubmit={this.submitForm(email, subject, comment)}>
              <fieldset>

                <fieldset className="form-group">
                  <input
                    className="form-control form-control-lg"
                    type="email"
                    placeholder="Email"
                    value={email}
                    onChange={this.changeEmail} />
                  <span hidden={!props.wrong_email}>{props.error_email}</span>
                </fieldset>

                <fieldset className="form-group">
                  <input
                    className="form-control form-control-lg"
                    type="text"
                    placeholder="Subject"
                    value={subject}
                    onChange={this.changeSubject} />
                  <span hidden={!props.wrong_subject}>{props.error_subject}</span>
                </fieldset>

                <fieldset className="form-group">
                  <input
                    className="form-control form-control-lg"
                    type="text"
                    placeholder="Comment"
                    value={comment}
                    onChange={this.changeComment} />
                  <span hidden={!props.wrong_comment}>{props.error_comment}</span>
                </fieldset>

                <button
                  className="btn btn-lg btn-primary pull-xs-right"
                  type="submit"
                  disabled={props.wrong_email || props.wrong_subject || props.wrong_comment}>
                  Send
                      </button>

              </fieldset>
            </form>
          </div>

        </div>
      </div>
    </div>
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(Contact);