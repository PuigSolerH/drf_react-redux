import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';
import gql from 'graphql-tag';

const superagent = superagentPromise(_superagent, global.Promise);

// const API_ROOT = 'https://conduit.productionready.io/api';
const API_ROOT = 'http://localhost:8000/api';

const encode = encodeURIComponent;
const responseBody = res => res.body;
// const responseBody = res => {
//   console.log('res',res.body);
//   res.body};

let token = null;
const tokenPlugin = req => {
  if (token) {
    req.set('authorization', `Token ${token}`);
  }
}

const requests = {
  del: url =>
    superagent.del(`${API_ROOT}${url}`).use(tokenPlugin).then(responseBody),
  get: url =>
    superagent.get(`${API_ROOT}${url}`).use(tokenPlugin).then(responseBody),
  put: (url, body) =>
    superagent.put(`${API_ROOT}${url}`, body).use(tokenPlugin).then(responseBody),
  post: (url, body) =>
    superagent.post(`${API_ROOT}${url}`, body).use(tokenPlugin).then(responseBody)
};

const Auth = {
  current: () =>
    requests.get('/user'),
  login: (email, password) =>
    requests.post('/users/login', { user: { email, password } }),
  register: (username, email, password) =>
    requests.post('/users', { user: { username, email, password } }),
  socialLogin: (username, email, id) =>
    requests.post('/socialusers', { user: { username, email, id } }),
  save: user =>
    requests.put('/user', { user })
};

const Tags = {
  getAll: () => requests.get('/tags')
};

const limit = (count, p) => `limit=${count}&offset=${p ? p * count : 0}`;
const omitSlug = article => Object.assign({}, article, { slug: undefined })
const Articles = {
  all: page =>
    requests.get(`/articles?${limit(10, page)}`),
  byAuthor: (author, page) =>
    requests.get(`/articles?author=${encode(author)}&${limit(5, page)}`),
  byTag: (tag, page) =>
    requests.get(`/articles?tag=${encode(tag)}&${limit(10, page)}`),
  del: slug =>
    requests.del(`/articles/${slug}`),
  favorite: slug =>
    requests.post(`/articles/${slug}/favorite`),
  favoritedBy: (author, page) =>
    requests.get(`/articles?favorited=${encode(author)}&${limit(5, page)}`),
  feed: () =>
    requests.get('/articles/feed?limit=10&offset=0'),
  get: slug =>
    requests.get(`/articles/${slug}`),
  unfavorite: slug =>
    requests.del(`/articles/${slug}/favorite`),
  update: article =>
    requests.put(`/articles/${article.slug}`, { article: omitSlug(article) }),
  create: article =>
    requests.post('/articles', { article })
};

const Comments = {
  create: (slug, comment) =>
    requests.post(`/articles/${slug}/comments`, { comment }),
  delete: (slug, commentId) =>
    requests.del(`/articles/${slug}/comments/${commentId}`),
  forArticle: slug =>
    requests.get(`/articles/${slug}/comments`)
};

const Profile = {
  follow: username =>
    requests.post(`/profiles/${username}/follow`),
  get: username =>
    requests.get(`/profiles/${username}`),
  unfollow: username =>
    requests.del(`/profiles/${username}/follow`)
};

const Contact = {
  contact: (email, subject, comment) =>
    requests.post('/contact', { data: { email, subject, comment } }),
};


const Deportes = {
  getAll: () => requests.get('/deportes')
};

const limit_equipos = (count, p) => `limit=${count}&offset=${p ? p * count : 0}`;
const omitSlug_equipos = equipo => Object.assign({}, equipo, { slug: undefined })
const Equipos = {
  all: page =>
    requests.get(`/equipos?${limit_equipos(10, page)}`),
  byUser: (user, page) =>
    requests.get(`/equipos?user=${encode(user)}&${limit_equipos(5, page)}`),
  byDeporte: (deporte, page) =>
    requests.get(`/equipos?deporte=${encode(deporte)}&${limit_equipos(10, page)}`),
  del: slug =>
    requests.del(`/equipos/${slug}`),
  // favorite: slug =>
  //   requests.post(`/equipos/${slug}/favorite`),
  // favoritedBy: (author, page) =>
  //   requests.get(`/equipos?favorited=${encode(author)}&${limit_equipos(5, page)}`),
  feed: () =>
    requests.get('/equipos/feed?limit=10&offset=0'),
  get: slug =>
    requests.get(`/equipos/${slug}`),
  // unfavorite: slug =>
  //   requests.del(`/equipos/${slug}/favorite`),
  update: equipo =>
    requests.put(`/equipos/${equipo.slug}`, { equipo: omitSlug_equipos(equipo) }),
  create: equipo =>
    requests.post('/equipos', { equipo })
};

const Noticias = {
  create: (slug, noticia) =>
    requests.post(`/equipos/${slug}/noticias`, { noticia }),
  delete: (slug, idNoticia) =>
    requests.del(`/equipos/${slug}/noticias/${idNoticia}`),
  forEquipo: slug =>
    requests.get(`/equipos/${slug}/noticias`),
  detailsNoticia: id => 
    requests.get(`/noticias/${id}`)
};

const GET_ALL_SPORTS = gql`
  {
    deportesAll {
      nombre
    }
  }
`;

const GET_EQUIPO_NEWS = gql`
  query Noticia($slug: String!) {
    equipoNoticia(slug: $slug) {
      id
      createdAt
      title
      body
      author {
        user {
          username
        }
        image
      }
    }
  }
`;

const GET_EQUIPO = gql`
  query Equipo($slug: String!) {
    equipo(slug: $slug) {
      id
      slug
      nombre
      presupuesto
      victorias
      empates
      derrotas
      puntos
      image
      deporte {
        nombre
      }
      user {
        user {
          username
        }
        image
      }
    }
  }
`;

const DELETE_NOTICIA = gql`
  mutation deleteNoticia($id: Int!) {
    deleteNoticia (noticiaPk: $id){
      noticia {
        id
      }
    }
  }
`;

export default {
  Articles,
  Auth,
  Comments,
  Profile,
  Tags,
  Contact,
  Deportes,
  Equipos,
  Noticias,
  GET_ALL_SPORTS,
  GET_EQUIPO_NEWS,
  GET_EQUIPO,
  DELETE_NOTICIA,
  setToken: _token => { token = _token; }
};
